from django.db import models
from ckeditor.fields import RichTextField


class Post(models.Model):
    CATEGORY = (
        ('ekopolkum', 'Ekonomi,Politik,Hukum'),
        ('soshum', 'Sosial,Humaniora'),
        ('saintek', 'Sains,Teknologi'),
        ('advertorial', 'Advertorial'),
        ('inspirasi', 'Inspirasi'),
        ('riset', 'Riset')
    )

    DISPLAY_STATUS = (
        ('P', 'Publish'),
        ('D', 'Draft'),
    )

    COMMENT_STATUS = (
        ('O', 'Open'),
        ('C', 'Closed'),
    )

    date_created = models.DateTimeField(auto_now_add=True, editable=False)
    date_modified = models.DateTimeField(auto_now_add=True)
    category = models.CharField(
        choices=CATEGORY,
        max_length=20
    )
    title = models.TextField()
    content = RichTextField()
    image = models.ImageField(upload_to='post/%Y/%m/%d',)
    status = models.CharField(
        choices=DISPLAY_STATUS,
        max_length=2
    )
    comment_status = models.CharField(
        choices=COMMENT_STATUS,
        max_length=2
    )
    author = models.CharField(max_length=255)
    contributor = models.CharField(max_length=255)
    editor = models.CharField(max_length=255)

    def __str__(self):
        return '%s' % (self.title)


class Gallery(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, editable=False)
    date_modified = models.DateTimeField(auto_now_add=True)
    title = models.TextField()
    content = RichTextField()
    photographer = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Galleries'

    def __str__(self):
        return '%s' % (self.title)


class ImageGallery(models.Model):
    post = models.ForeignKey(
        Gallery, related_name='images', on_delete=models.CASCADE)
    file = models.ImageField(upload_to='gallery/%Y/%m/%d')
    position = models.PositiveSmallIntegerField(default=0)

    class Meta:
        ordering = ['position']
        verbose_name_plural = 'Image Galleries'

    def __str__(self):
        return '%s' % (self.file)


class Comment(models.Model):
    post = models.ForeignKey(
        'Post',
        on_delete=models.CASCADE,
    )
    date_created = models.DateTimeField(auto_now_add=True, editable=False)
    content = models.TextField()
    author_ip = models.CharField(max_length=100)
    author_email = models.EmailField()


class Grafis(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, editable=False)
    image = models.ImageField(upload_to='grafis/%Y/%m/%d')
    tag = models.CharField(max_length=255)

    def __str__(self):
        return '%s' % (self.tag)

    class Meta:
        verbose_name_plural = "Grafis"


class Gerbatama(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, editable=False)
    image = models.ImageField(upload_to='gerbatama/%Y/%m/%d')
    urls = models.URLField()

    def __str__(self):
        return 'Gerbatama %s' % (self.id)
