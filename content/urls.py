from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from content.views import *


urlpatterns = [
    path('', index, name="homepage"),
    path('about/', about, name="about"),
    path('category/<name>/', category, name="category"),
    path('news/<title>/', news, name="news"),
    path('gallery/', gallery, name="gallery"),
    path('gallery/<title>/', exhibition, name="exhibition"),
    path('gerbatama/', gerbatama, name="gerbatama"),
    path('grafis/', grafis, name="grafis"),
    path('comment/submit/', submit_comment, name="comment"),
    path('comment/<title>/', retrieve_comment, name="comment"),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
