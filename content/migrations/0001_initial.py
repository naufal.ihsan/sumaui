# Generated by Django 2.2 on 2019-07-05 12:02

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now_add=True)),
                ('title', models.TextField()),
                ('content', ckeditor.fields.RichTextField()),
                ('photographer', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Galleries',
            },
        ),
        migrations.CreateModel(
            name='Gerbatama',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('image', models.ImageField(upload_to='gerbatama/%Y/%m/%d')),
                ('urls', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='Grafis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('image', models.ImageField(upload_to='grafis/%Y/%m/%d')),
                ('tag', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Grafis',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now_add=True)),
                ('category', models.CharField(choices=[('ekopolkum', 'Ekonomi,Politik,Hukum'), ('soshum', 'Sosial,Humaniora'), ('saintek', 'Sains,Teknologi'), ('advertorial', 'Advertorial'), ('inspirasi', 'Inspirasi'), ('riset', 'Riset')], max_length=20)),
                ('title', models.TextField()),
                ('content', ckeditor.fields.RichTextField()),
                ('image', models.ImageField(upload_to='post/%Y/%m/%d')),
                ('status', models.CharField(choices=[('P', 'Publish'), ('D', 'Draft')], max_length=2)),
                ('comment_status', models.CharField(choices=[('O', 'Open'), ('C', 'Closed')], max_length=2)),
                ('author', models.CharField(max_length=255)),
                ('contributor', models.CharField(max_length=255)),
                ('editor', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ImageGallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.ImageField(upload_to='gallery/%Y/%m/%d')),
                ('position', models.PositiveSmallIntegerField(default=0)),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='content.Gallery')),
            ],
            options={
                'verbose_name_plural': 'Image Galleries',
                'ordering': ['position'],
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField()),
                ('author_ip', models.CharField(max_length=100)),
                ('author_email', models.EmailField(max_length=254)),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='content.Post')),
            ],
        ),
    ]
