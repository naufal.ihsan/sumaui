from django.contrib import admin
from content.models import Post,Gerbatama,Grafis,Gallery,ImageGallery

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_filter = (('category',))
admin.site.register(Post,PostAdmin)
admin.site.register(Gerbatama)
admin.site.register(Grafis)

class ImageGalleryInline(admin.StackedInline):
    model = ImageGallery
    extra = 1

class GalleryAdmin(admin.ModelAdmin):
    inlines = [ImageGalleryInline]

    def save_model(self, request, obj, form, change):
        obj.save()

        for afile in request.FILES.getlist('photos_multiple'):
            obj.images.create(file=afile)

admin.site.register(Gallery,GalleryAdmin)