from django.shortcuts import render
from django.http import JsonResponse
from django.core.paginator import Paginator
from django.core import serializers
from content.models import Post, Comment, Gerbatama, Grafis, Gallery, ImageGallery

# Create your views here.
response = {}

def index(request):
    headlines = Post.objects.order_by('date_created').reverse()[:3]
    response['headlines'] = headlines

    galleries = Gallery.objects.order_by('date_created').reverse()[:3]
    recent_gallery = list()
    for gallery in galleries:
        image = ImageGallery.objects.filter(post = gallery)[0]
        recent_gallery.append(image)
    
    response['galleries'] = recent_gallery

    grafis = Grafis.objects.order_by('date_created').reverse()[:2]
    response['grafis'] = grafis

    return render(request, 'content/index.html', response)

def about(request):
    return render(request, 'content/about.html', response)

def category(request,name):
    post_list = Post.objects.filter(category=name).order_by('date_created').reverse()
    paginator = Paginator(post_list, 5)
    page = request.GET.get('page')
    posts = paginator.get_page(page)

    response['category'] = name
    response['posts'] = post_list
    response['pages'] = posts

    return render(request, 'content/category.html', response)

def gallery(request):
    gallery_list = Gallery.objects.all().order_by('date_created').reverse()
    paginator = Paginator(gallery_list, 5)
    page = request.GET.get('page')
    galleries = paginator.get_page(page)

    combines = list()

    for gallery in gallery_list:
        images = ImageGallery.objects.filter(post=gallery)
        combines.append((gallery,images))
        
    response['category'] = "gallery"
    response['posts'] = combines
    response['pages'] = galleries

    return render(request, 'content/category.html', response)

def exhibition(request,title):
    title = title.replace('-',' ')
    post = Gallery.objects.get(title=title)
    images = ImageGallery.objects.filter(post=post)
    response['section'] = 'exhibition'
    response['post'] = post
    response['images'] = images


    related = Gallery.objects.filter(photographer=post.photographer).exclude(pk=post.pk)[:3]
    response['related'] = related   

    return render(request, 'content/news.html', response)

def news(request,title):
    title = title.replace('-',' ')
    post = Post.objects.get(title=title)
    response['post'] = post
    response['section'] = post.category

    related = Post.objects.filter(category=post.category).exclude(pk=post.pk)[:3]
    response['related'] = related   

    return render(request, 'content/news.html', response)

def gerbatama(request):
    gerbatamas = Gerbatama.objects.all()
    response['objects'] = gerbatamas
    response['section'] = "gerbatama"

    return render(request, 'content/gallery.html', response)

def grafis(request):
    grafis = Grafis.objects.all()
    response['objects'] = grafis
    response['section'] = "grafis"

    return render(request, 'content/gallery.html', response)


def submit_comment(request):
    if request.method == "POST":
        title = request.POST['title']
        email = request.POST['email']
        content = request.POST['content']
        author_ip = request.META['REMOTE_ADDR']
        post = Post.objects.get(title=title)
        comment = Comment(post=post,content=content,author_ip=author_ip,author_email=email)
        comment.save()
        return JsonResponse({"response":"success"})
    else:
        return JsonResponse({"response":"failed"})

def retrieve_comment(request,title):
    post = Post.objects.get(title=title)
    data = serializers.serialize(
        'json',
        Comment.objects.filter(post=post)
    )

    return JsonResponse(data ,safe=False)    