from django import template

register = template.Library()

@register.filter
def slug(value):
    return value.replace(' ', '-')

@register.filter
def row(value):
    return value % 3 == 0

@register.filter
def endrow(value):
    return (value+1) % 3 == 0 and value > 0

@register.filter
def closer(value):
    return value % 3 != 0